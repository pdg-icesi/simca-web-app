import React, {Component} from 'react';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import promise from "redux-promise";

//Custom imports
import AppRouter from './appRouter';
import reducers from './reducers';

import './styles/app.css';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

class App extends Component {

    render(){
      return (
        <Provider store={createStoreWithMiddleware(reducers)}>
            <AppRouter />
        </Provider>
      );
    }

}

export default App;
