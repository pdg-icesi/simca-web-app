import React, {Component} from 'react';

import Logo from '../../assets/logotipo.svg';
import '../../styles/header.css';

class Header extends Component {

    render(){
        return(
            <div className="header">
                <img src={Logo} alt="logotipo" className="header-image"/>
            </div>
        );        
    }

}

export default Header;