import React, {Component} from 'react';

import '../../styles/home.css';

class Home extends Component {

    render(){
        return(
            <div className="home-container">
                <h2>¿Qué es SIMCA?</h2>
                <h5>Sistema de Medición de la Calidad del Aire</h5>
            </div>
        );
    }

}

export default Home;