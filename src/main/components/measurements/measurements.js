import React, {Component} from 'react';

import Filters from './filters';
import MeasurementsList from './measurements_list';

import '../../styles/measurements.css';

class Measurements extends Component {

    render(){
        return(
            <div className="measurements">
                <Filters/>
                <MeasurementsList/>
            </div>
        );
    }
}

export default Measurements;