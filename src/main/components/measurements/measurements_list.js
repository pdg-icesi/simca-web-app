import React, { Component } from 'react';

import MeasurementCard from './measurement_card';

class MeasurementsList extends Component {

    render() {
        return (
            <div className="measurements-list custom-scrollbar">
                <MeasurementCard />
                <MeasurementCard />
                <MeasurementCard />
                <MeasurementCard />
                <MeasurementCard />
                <MeasurementCard />
                <MeasurementCard />
                <MeasurementCard />
            </div>
        );
    }

}

export default MeasurementsList;