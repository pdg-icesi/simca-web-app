import React, { Component } from 'react';

import '../../styles/filters.css';

class Filters extends Component {

    render() {
        return (

            <div className="filters-box">
                <p>Filtrar mediciones por:</p>
                <input id="chk_date" type="checkbox" name="chk_group" value="value1" />
                <label htmlFor="chk_date">Fecha</label><br />
                <input id="chk_place" type="checkbox" name="chk_group" value="value2" />
                <label htmlFor="chk_place">Lugar</label><br />
                <input id="chk_pollutant" type="checkbox" name="chk_group" value="value3" />
                <label htmlFor="chk_pollutant">Contaminante</label>
            </div>
        );
    }

}

export default Filters;