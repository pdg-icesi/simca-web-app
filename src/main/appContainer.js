import React, { Component } from 'react';
import { Switch, Route, Link } from "react-router-dom";
import { slide as Menu } from 'react-burger-menu';

//Custome imports
import Header from './components/header/header';
import Home from './components/home/home';
import Measurements from './components/measurements/measurements';
import Places from './components/places/places';
import Pollutants from './components/pollutants/pollutants';
import About from './components/about/about';

class AppContainer extends Component {

    constructor(props){
        super(props);

        this.state= {
            open:false
        };

        this.hideMenu= this.hideMenu.bind(this);
    }


    hideMenu(){
        this.setState(
            {
                open:false
            }
        );
    }

    getMenu(){

        return(
            <Menu id="push" pageWrapId={"page-wrap"} outerContainerId={"outer-container"} isOpen={this.state.open}>
                <Link id="home" className="menu-item" to="/" onClick={this.hideMenu}>Home</Link>
                <Link id="about" className="menu-item" to="/measurements" onClick={this.hideMenu}>Mediciones</Link>
                <Link id="contact" className="menu-item" to="/about" onClick={this.hideMenu}>Acerca de</Link>
            </Menu>
        );
    }

    render() {

        const { match } = this.props;

        return (
            <div className="app-container" id="outer-container" style={{height: '100%'}}>
                {this.getMenu()}
                <Header/>
                <div id="page-wrap" className="app-content">
                    <Switch>
                        <Route path={`${match.url}home`} component={Home} />
                        <Route path={`${match.url}measurements`} component={Measurements} />
                        <Route path={`${match.url}places`} component={Places} />
                        <Route path={`${match.url}pollutants`} component={Pollutants} />
                        <Route path={`${match.url}about`} component={About} />
                        <Route path={`${match.url}`} component={Home} />
                    </Switch>
                </div>
            </div>
        );
    }
}

export default AppContainer;