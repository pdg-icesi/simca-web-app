import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

//Custom imports
import AppContainer from './appContainer';

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route path="/" component={AppContainer} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
